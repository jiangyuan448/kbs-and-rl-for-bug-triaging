import os,re
import pandas as pd
import numpy as np

def rename():
    for root,dirs,files in os.walk('./'):
        for file in files:
            if file.endswith('.result'):
                src_file_name=os.path.join(root,file)
                file_part=re.split('[_.]',file)
                dest_file_name=os.path.join(root, '%s_%s_%s_%s_%s.result' % (file_part[0], file_part[1], file_part[2], file_part[3],0.001))
                os.rename(src_file_name,dest_file_name)

def remove():

    for root, dirs, files in os.walk('./'):
        for file in files:
            if file.endswith('.result'):
                file_splits = re.split('[_,.]', file)
                if len(file_splits) == 5:
                    os.remove(os.path.join(root, file))

# chose the best metric value from dir
def parameter_chose(dir):
    if dir.startswith('eclipse'):
        project = 'eclipse'
    elif dir.startswith('mozilla'):
        project = 'mozilla'
    else:
        project = 'apache'
    result_path = os.path.join('./', project.upper(), dir)
    best_result = 0
    best_result_file = ''
    for file in os.listdir(result_path):
        if os.path.isdir(file) or file.startswith('.'):
            continue
        print(file)
        result=pd.read_csv(os.path.join(result_path,file))

        temp_metric_float=result['top@1'].astype('float64', copy=False)
        evaluate_metric=np.mean(temp_metric_float)

        if best_result <evaluate_metric:
            best_result=evaluate_metric
            best_result_file=file
    print('file:%s, value: %s'%(best_result_file,best_result))

# chose the best result for specific model in a dir
def best_result2dir(dir, model, metric):
    if dir.startswith('eclipse'):
        project = 'eclipse'
    elif dir.startswith('mozilla'):
        project = 'mozilla'
    else:
        project = 'apache'
    result_path = os.path.join('./', project.upper(), dir)
    best_result_metric = 0
    best_result = ''
    for file in os.listdir(result_path):
        if os.path.isdir(file):
            continue
        if not file.startswith(model):
            continue

        result=pd.read_csv(os.path.join(result_path,file),index_col=0)
        temp_metric_float=result[metric].astype('float64', copy=True)
        evaluate_metric=np.mean(temp_metric_float)

        if best_result_metric <evaluate_metric:
            best_result_metric=evaluate_metric
            best_result=result
    # print('file:%s, value: %s'%(best_result_file,best_result_metric))
    return best_result.astype('float64', copy=True,errors='ignore')

def average_ten_fold(project,model,filter=False):
    # project='apache'
    # model='TransE'
    file_name = project + '_' + model + '_' + str(filter) + '.csv'
    store_path=os.path.join('./Result',file_name)
    if os.path.exists(store_path):
        return

    metric='mrr'
    accum_sum=0
    dir=os.path.join('./',project.upper())
    for f in os.listdir(dir):
        if os.path.isdir(os.path.join(dir,f)):
            # filter=true represent filter result
            if not filter:
                if 'f' in f:
                    continue
            else:
                if not 'f' in f:
                    continue

            dir_best_result=best_result2dir(f,model,metric)
            if type(accum_sum)==int:
                accum_sum=dir_best_result
            else:
                accum_sum += dir_best_result
    # print(accum_sum)
    fold=10
    if project=='apache':
        fold=2
    accum_sum=accum_sum/fold

    accum_sum.to_csv(store_path)

    print(accum_sum)

# result statistic
def statistic():
    projects=['eclipse','mozilla','apache']
    models=['PTITransE','TransE','TransD','TransH']
    filters=[True,False]
    for project in projects:
        for model in models:
            for filter in filters:
                average_ten_fold(project,model,filter)




if __name__ == '__main__':
    # dir='eclipse_1'
    # parameter_chose(dir)
    # remove()
    statistic()