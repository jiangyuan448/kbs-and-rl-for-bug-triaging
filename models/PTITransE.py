import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
from Model import *
from torch.autograd import Variable
import json
import pickle

class PTITransE(Model):

	def __init__(self, config):
		super(PTITransE,self).__init__(config)
		self.ent_embeddings=nn.Embedding(config.entTotal,config.hidden_size)

		self.rel_embeddings=nn.Embedding(config.relTotal,config.hidden_size)
		# self.rel_matrix = nn.Embedding(config.relTotal, config.hidden_size)
		entity_path = config.in_path + 'entity2id.txt'
		self.bug_num=None
		self.entity = self.read_entity(entity_path)
		self.des_embeddings = nn.Embedding(config.entTotal, config.hidden_size)

		self.ent_embeddings_temp=None
		self.des_embeddings_temp=None

		self.init_weights()
		self.entity_id_to_idx = {}
		self.get_word2vec(config.in_path,config.ent_size)


	def init_weights(self):
		# nn.init.xavier_uniform(self.ent_embeddings.weight.data)
		nn.init.xavier_uniform(self.ent_embeddings.weight.data)
		nn.init.xavier_uniform(self.rel_embeddings.weight.data)
		# nn.init.xavier_uniform(self.des_embeddings.weight.data)
		# add text information
		self.des_embeddings.weight.data=self.ent_embeddings.weight.data.clone()

		self.ent_embeddings_temp=Variable(self.ent_embeddings.weight.data.clone(),requires_grad=False)
		self.des_embeddings_temp=Variable(self.des_embeddings.weight.data.clone(),requires_grad=False)

	def read_entity(self,file_path, has_header=True):
		entity = {}
		with open(file_path) as fp:
			if has_header is True:
				fp.readline()
			for line in fp:
				line = line.replace('\n', '')
				line = line.split(' ')
				entity[int(line[-1])] = ' '.join(line[0:-1])
		count=0
		for key,value in entity.items():
			if value.startswith('bug'):
				count+=1
		self.bug_num=count
		return entity

	def get_word2vec(self, path,dim):
		word_matrix = pickle.load(open(path + str(dim)+('_item_matrix'), 'rb'))
		# read 'entity' file
		index=0
		# key: entity id, value: bug id(e.g. bug3550)
		for key,value in self.entity.items():
			if value in word_matrix:
				self.des_embeddings.weight.data[key]=torch.FloatTensor(word_matrix[value])
				self.entity_id_to_idx[key]=index

	def _calc(self, h, t, r):
		return torch.abs(h + r - t)


	def loss_func(self, p_score, n_score):
		criterion = nn.MarginRankingLoss(self.config.margin, False).cuda()
		y = Variable(torch.Tensor([-1])).cuda()
		loss = criterion(p_score, n_score, y)
		return loss

	def get_des_embedding(self,pos_h_np,pos_t_np,neg_h_np,neg_t_np):

		bug_entity=self.entity_id_to_idx.keys()
		# subscript of bug entity in pos_h or pos_t or neg_h or neg_t

		pos_h_index = [i for i,x in enumerate(pos_h_np) if x not in bug_entity]
		pos_t_index = [i for i,x in enumerate(pos_t_np) if x not in bug_entity]

		neg_h_index = [i for i,x in enumerate(neg_h_np) if x not in bug_entity]
		neg_t_index = [i for i,x in enumerate(neg_t_np) if x not in bug_entity]

		pos_index=list(set(pos_h_index).intersection(set(pos_t_index)))
		pos_index.sort()

		neg_index=list(set(neg_h_index).intersection(set(neg_t_index)))
		neg_index.sort()

		index=list(set(pos_index).union(set(neg_index)))
		all={
			'index':index,
			'not_pos_index':pos_index,
			'not_neg_index':neg_index,
		}
		return all



	def forward(self):
		pos_h, pos_t, pos_r = self.get_postive_instance()
		neg_h, neg_t, neg_r = self.get_negtive_instance()

		p_h=self.ent_embeddings(pos_h)
		p_t=self.ent_embeddings(pos_t)
		p_r=self.rel_embeddings(pos_r)
		n_h=self.ent_embeddings(neg_h)
		n_t=self.ent_embeddings(neg_t)
		n_r=self.rel_embeddings(neg_r)

		# pos_h_np=pos_h.data.cpu()
		# pos_t_np=pos_t.data.cpu()
		# neg_h_np=neg_h.data.cpu()
		# neg_t_np=neg_t.data.cpu()

		# all=self.get_des_embedding(pos_h_np,pos_t_np,neg_h_np,neg_t_np)

		_p_score = self._calc(p_h, p_t, p_r)
		_n_score = self._calc(n_h, n_t, n_r)
		p_score=torch.sum(_p_score,1)
		n_score=torch.sum(_n_score,1)

		p_des_h = self.des_embeddings(pos_h)
		p_des_t = self.des_embeddings(pos_t)
		n_des_h = self.des_embeddings(neg_h)
		n_des_t = self.des_embeddings(neg_t)
		_p_des_score = self._calc(p_des_h, p_des_t, p_r)
		_n_des_score = self._calc(n_des_h, n_des_t, n_r)

		p_ent_temp_h = self.ent_embeddings_temp[pos_h.cpu()]
		p_ent_temp_t = self.ent_embeddings_temp[pos_t.cpu()]
		n_ent_temp_h = self.ent_embeddings_temp[neg_h.cpu()]
		n_ent_temp_t = self.ent_embeddings_temp[neg_t.cpu()]

		p_des_temp_h=self.des_embeddings_temp[pos_h.cpu()]
		p_des_temp_t = self.des_embeddings_temp[pos_t.cpu()]
		n_des_temp_h = self.des_embeddings_temp[neg_h.cpu()]
		n_des_temp_t = self.des_embeddings_temp[neg_t.cpu()]

		ph_index = torch.sum(p_ent_temp_h-p_des_temp_h,1)
		pt_index = torch.sum(p_ent_temp_t - p_des_temp_t, 1)
		nh_index = torch.sum(n_ent_temp_h - n_des_temp_h, 1)
		nt_index = torch.sum(n_ent_temp_t - n_des_temp_t, 1)

		index=torch.sum(torch.cat((ph_index.view(pos_h.size(0),-1),pt_index.view(pos_h.size(0),-1),nh_index.view(pos_h.size(0),-1),nt_index.view(pos_h.size(0),-1)),1),1)

		index=torch.gt(torch.abs(index),0.000001)
		index=index.float().cuda().view(pos_h.size(0),-1)
		# print 'index'
		# print index

		p_des_score = torch.sum(_p_des_score, 1).view(pos_h.size(0),-1)
		n_des_score = torch.sum(_n_des_score, 1).view(pos_h.size(0),-1)

		p_des_score=torch.prod(torch.cat((p_des_score,index),1),1)
		n_des_score = torch.prod(torch.cat((n_des_score, index),1),1)

		p_score = p_score+ p_des_score
		n_score = n_score+ n_des_score
		loss=self.loss_func(p_score, n_score)
		return loss


	def predict(self, predict_h, predict_t, predict_r):
		p_h = self.ent_embeddings(Variable(torch.from_numpy(predict_h)).cuda())
		p_t = self.ent_embeddings(Variable(torch.from_numpy(predict_t)).cuda())
		p_r = self.rel_embeddings(Variable(torch.from_numpy(predict_r)).cuda())
		_p_score = self._calc(p_h, p_t, p_r)
		p_score = torch.sum(_p_score, 1)

		# p_d_h = self.des_embeddings(Variable(torch.from_numpy(predict_h)).cuda())
		# p_d_t = self.des_embeddings(Variable(torch.from_numpy(predict_t)).cuda())
		# _p_d_score = self._calc(p_d_h, p_d_t, p_r)
		# p_score += torch.sum(_p_d_score, 1)

		return p_score.cpu()

