
# PTITransE
A new representation learning model PTITransE for knowledge bases, which extends TransE via enhancing the embeddings with textual entity descriptions and is more suitable for bug triaging.

## Overview

This is an Efficient implementation based on PyTorch for knowledge representation learning (KRL). We use C++ to implement some underlying operations such as  negative sampling. For each specific model, it is implemented by PyTorch with Python interfaces so that there is a convenient platform to run models on GPUs.


## Installation

1. Install PyTorch

2. Clone the repository:

	$ git clone https://gitlab.com/jiangyuan448/kbs-and-rl-for-bug-triaging.git
	
	$ cd kbs-and-rl-for-bug-triaging

3. Compile C++ files
	
	$ bash make.sh

## Data collection

We collect 320,639 bug reports with the status *close* and *fixed* from three open-source software projects (i.e. Eclipse, Mozilla and Apache) to assess the performance of our proposed approach. For each dataset, we collect the basic information of bug reports, including the number of collected bug reports (\# of bug reports), unique developers in the collected bug reports (\# of unique developers), unique fixers (\# of unique fixers), unique reporters (\# of unique reporters), different products (\# of products) and different components (\# of components).

The collected bug reports from three projects are available at https://drive.google.com/drive/folders/1Tlmybw2BTZLT-yP_pQndTTZa6-Qp2md4?usp=sharing. 

## Data preprocessing

To obtain the data used for training model, we first parse each bug report to extract the entities and relations within it, and then use these entities and relations to construct triplets in the form of (**head entity**, **relation**,  **tail entity**) based on the fact involving them.  For brevity, in the following,  we abbreviate the triplets as **(e1, e2, rel)**, where **e1**, **e2** and **rel** represent **head entity**, **tail entity** and **relation** respectively.
For textual information, we only consider the brief and detailed description of bug reports, i.e.  **summary** and **description**, following the previous researches. Then we extract them by the aid of the field tags labelled by the reporters and store them as text. 

The constructed datasets for three projects (i.e., [ Apache, Eclipse and Mozilla](https://drive.google.com/drive/folders/14amK1sxzxw8DWXxhkgHs4mfnmtshwI--?usp=sharing)) can be downloaded from google drive since the amount of data is too large. 

## Introduction of training and testing datasets

To further prepare the data for our experiments, the bug reports from each project in datasets are initially sorted in chronological order, and then divided into 11 disjoint equal-sized folds  **fold_1** ,  **fold_2**..., **fold_11** , where  **fold_11**  contains the newest reports, whereas  **fold_1 ** is the oldest. Note that the bug reports from Apache are divided into three folds, due to the smaller size of the project. We first conduct training to learn the representation for each entity and each relationship on   **fold_k ** and then evaluate the performance of leaned representation using link prediction on  test dataset **fold_k+1 **, for all **k<=10**, which can guarantee that the embeddings of **developer** entities and the **fix** relation used in link prediction to predict the suitable fixers for the bug reports in the test dataset are all learned from the most recent bug reports.  In convenience of  testing, we maintain a folder for each experiment and each folder contains training and testing dataset ﬁles.  

* For training, datasets contain three files:

  train2id.txt: training file, the first line is the number of triples for training. Then the following lines are all in the format **(e1, e2, rel)** which indicates there is a relation **rel** between **e1** and **e2** .
  **Note that train2id.txt contains ids from entitiy2id.txt and relation2id.txt instead of the names of the entities and relations. If you use your own datasets, please check the format of your training file. Files in the wrong format may cause segmentation fault.**

  entity2id.txt: all entities and corresponding ids, one per line. The first line is the number of entities.

  relation2id.txt: all relations and corresponding ids, one per line. The first line is the number of relations.

  *_item_matrix: the continuous bag-of-words representation for textual information of bug reports, where * represent the dimension of word vector.

* For testing, datasets contain additional one file (totally five files):

  test2id.txt: testing file, the first line is the number of triples for testing. Then the following lines are all in the format **(e1, e2, rel)** .

  

## Quickstart

### Training

To compute a knowledge graph embedding, first import datasets and set configure parameters for training, then train models and export results. For instance, we write an example to train PTITransE:
	

	import config
	import models
	import json
	import os


	con = config.Config()
	#Input training files from benchmarks/FB15K/ folder.
	con.set_in_path("./benchmarks/Apache1/")
	#True: Input test files from the same folder.
	con.set_log_on(1)
	con.set_work_threads(8)
	con.set_train_times(200)
	con.set_nbatches(100)
	con.set_alpha(0.001)
	con.set_bern(0)
	con.set_dimension(100)
	con.set_margin(1.0)
	con.set_ent_neg_rate(1)
	con.set_rel_neg_rate(0)
	con.set_opt_method("SGD")
	#Model parameters will be exported via torch.save() automatically.
	con.set_export_files("./Apache/model.vec.pt")
	#Model parameters will be exported to json files automatically.
	con.set_out_files("./Apache/embedding.vec.json")
	con.init()
	con.set_model(models.PTITransE)
	con.run()


#### Step 1: Import datasets

	con.set_in_path("./benchmarks/Apache1/")
	
	con.set_work_threads(8)

We can allocate several threads to sample positive and negative cases.


#### Step 2: Set configure parameters for training.

	con.set_train_times(500)
	con.set_nbatches(100)
	con.set_alpha(0.5)
	con.set_dimension(200)
	con.set_margin(1)

We set essential parameters, including the data traversing rounds, learning rate, batch size, and dimensions of entity and relation embeddings.

	con.set_bern(0)
	con.set_ent_neg_rate(1)
	con.set_rel_neg_rate(0)

For negative sampling, we can corrupt entities and relations to construct negative triples. set\_bern(0) will use the traditional sampling method, and set\_bern(1) will use the method in (Wang et al. 2014) denoted as "bern".
	
	con.set_optimizer("SGD")

We can select a proper gradient descent optimization algorithm to train models.

#### Step 3: Export results

	con.set_export_files("./res/model.vec.pt")
	
	con.set_out_files("./res/embedding.vec.json")

Model parameters will be exported via torch.save() automatically every few rounds. Also, they will be exported to json files finally. 

#### Step 4: Train models

	con.init()
	con.set_model(models.TransE)
	con.run()

We set the knowledge graph embedding model and start the training process.

### Testing

#### Link Prediction

Link prediction aims to predict the missing h or t for a relation fact triple (h, r, t). For each position of missing entity,  a common practice is to rank a set of candidate entities from the knowledge graph, instead of only giving one best result. Unlike the previous link prediction, we only treat head entity in the unseen triplets as target entity rather than both of head and tail entities for bug triaging task. Thus, the **developer** in the triplet of (**developer**, **fix**, **bug**) is the target entity to be predicted by the representation learning model.

For each test triple (**developer**, **fix**, **bug**), we replace the **head** entity by all entities in the knowledge graph, and rank these entities in descending order of similarity scores calculated by score function fr. we use two measures as our evaluation metric:

* **MR** : mean rank of correct entities; 
* **MRR**: the average of the reciprocal ranks of correct entities; 
* **Hit@N** : proportion of correct entities in top-N ranked entities.



### Getting the embedding matrix

There are four approaches to get the embedding matrix.

(1) Set import files and OpenKE will automatically load models via torch.load().

	import json
	import numpy as py
	import config
	import models
	con = config.Config()
	con.set_in_path("./benchmarks/Apache1/")
	con.test_link_prediction(True)
	con.test_triple_classification(True)
	con.set_work_threads(4)
	con.set_dimension(100)
	con.set_import_files("./res/model.vec.pt")
	con.init()
	con.set_model(models.PTITransE)
	# Get the embeddings (numpy.array)
	embeddings = con.get_parameters("numpy")
	# Get the embeddings (python list)
	embeddings = con.get_parameters()

(2) Read model parameters from json files and manually load parameters.

	import json
	import numpy as py
	import config
	import models
	con = config.Config()
	con.set_in_path("./benchmarks/Apache1/")
	con.test_link_prediction(True)
	con.test_triple_classification(True)
	con.set_work_threads(4)
	con.set_dimension(100)
	con.init()
	con.set_model(models.PTITransE)
	f = open("./res/embedding.vec.json", "r")
	embeddings = json.loads(f.read())
	f.close()

(3) Manually load models via torch.load().

	con = config.Config()
	con.set_in_path("./benchmarks/Apache1/")
	con.test_link_prediction(True)
	con.test_triple_classification(True)
	con.set_work_threads(4)
	con.set_dimension(100)
	con.init()
	con.set_model(models.PTITransE)
	con.import_variables("./res/model.vec.pt")
	# Get the embeddings (numpy.array)
	embeddings = con.get_parameters("numpy")
	# Get the embeddings (python list)
	embeddings = con.get_parameters()

(4) Immediately get the embeddings after training the model.

	...
	...
	...
	#Models will be exported via tf.Saver() automatically.
	con.set_export_files("./res/model.vec.pt")
	#Model parameters will be exported to json files automatically.
	con.set_out_files("./res/embedding.vec.json")
	#Initialize experimental settings.
	con.init()
	#Set the knowledge embedding model
	con.set_model(models.PTITransE)
	#Train the model.
	con.run()
	#Get the embeddings (numpy.array)
	embeddings = con.get_parameters("numpy")
	#Get the embeddings (python list)
	embeddings = con.get_parameters()

## Interfaces

### Config

	class Config(object):
			
		#To set the learning rate
		def set_alpha(alpha = 0.001)
		
		#To set the degree of the regularization on the parameters
		def set_lmbda(lmbda = 0.0)
		
		#To set the gradient descent optimization algorithm (SGD, Adagrad, Adadelta, Adam)
		def set_optimizer(optimizer = "SGD")
		
		#To set the data traversing rounds
		def set_train_times(self, times)
		
		#To split the training triples into several batches, nbatches is the number of batches
		def set_nbatches(nbatches = 100)
		
		#To set the margin for the loss function
		def set_margin(margin = 1.0)
		
		#To set the dimensions of the entities and relations at the same time
		def set_dimension(dim)
		
		#To set the dimensions of the entities
		def set_ent_dimension(self, dim)
		
		#To set the dimensions of the relations
		def set_rel_dimension(self, dim)
		
		#To allocate threads for each batch sampling
		def set_work_threads(threads = 1)
		
		#To set negative sampling algorithms, unif (bern = 0) or bern (bern = 1)
		def set_bern(bern = 1)
		
		#For each positive triple, we construct rate negative triples by corrupt the entity
		def set_ent_neg_rate(rate = 1)
		
		#For each positive triple, we construct rate negative triples by corrupt the relation
		def set_rel_neg_rate(rate = 0)
		
		#To sample a batch of training triples, including positive and negative ones.
		def sampling()
	
		#To import dataset from the benchmark folder
		def set_in_path(self, path)
		
		#To export model parameters to json files when training completed
		def set_out_files(self, path)
		
		#To set the import files, all parameters can be restored from the import files
		def set_import_files(self, path)
		
		#To set the export file of model paramters, and export results every few rounds
		def set_export_files(self, path, steps = 0)
	
		#To export results every few rounds
		def set_export_steps(self, steps)
	
		#To save model via torch.save()
		def save_pytorch(self)
	
		#To restore model via torch.load()
		def restore_pytorch(self)
	
		#To export model paramters, when path is none, equivalent to save_tensorflow()
		def export_variables(self, path = None)
	
		#To import model paramters, when path is none, equivalent to restore_tensorflow()
		def import_variables(self, path = None)
		
		#To export model paramters to designated path
		def save_parameters(self, path = None)
	
		#To manually load parameters which are read from json files
		def set_parameters(self, lists)
		
		#To get model paramters, if using mode "numpy", you can get np.array , else you can get python lists
		def get_parameters(self, mode = "numpy")
	 
		#To set the knowledge embedding model
		def set_model(model)
		
		#The framework will print loss values during training if flag = 1
		def set_log_on(flag = 1)
	
		#This is essential when testing
		def test_link_prediction(True)

Note that model paramters can be loaded only when all the configuration paramters are set.

### Model

	class Model(object)
	
		# in_batch = True, return [positive_head, positive_tail, positive_relation]
		# The shape of positive_head is [batch_size, 1]
		# in_batch = False, return [positive_head, positive_tail, positive_relation]
		# The shape of positive_head is [batch_size]
		get_positive_instance(in_batch = True)
		
		# in_batch = True, return [negative_head, negative_tail, negative_relation]
		# The shape of positive_head is [batch_size, negative_ent_rate + negative_rel_rate]
		# in_batch = False, return [negative_head, negative_tail, negative_relation]
		# The shape of positive_head is [(negative_ent_rate + negative_rel_rate) * batch_size]		
		get_negative_instance(in_batch = True)
	
		# in_batch = True, return all training instances with the shape [batch_size, (1 + negative_ent_rate + negative_rel_rate)]
		# in_batch = False, return all training instances with the shape [(negative_ent_rate + negative_rel_rate + 1) * batch_size]
		def get_all_instance(in_batch = False)
	
		# in_batch = True, return all training labels with the shape [batch_size, (1 + negative_ent_rate + negative_rel_rate)]
		# in_batch = False, return all training labels with the shape [(negative_ent_rate + negative_rel_rate + 1) * batch_size]
		# The positive triples are labeled as 1, and the negative triples are labeled as -1
		def get_all_labels(in_batch = False)
		
		#To calulate the loss
		def forward(self)
	
		# To define loss functions for knowledge embedding models
		def loss_func()
		
		# To define the prediction functions for knowledge embedding models
		def predict(self)
	
		def __init__(config)
	
	#The implementation for TransE
	class TransE(Model)
	
	#The implementation for TransH	
	class TransH(Model)
	
	#The implementation for TransR
	class TransR(Model)
	
	#The implementation for TransD
	class TransD(Model)
	
	#The implementation for PTITransE
	class PTITransE(Model)


​	

### Result statistic

```
# chose the best result for specific model in a dir
def best_result2dir(dir, model, metric):
    if dir.startswith('eclipse'):
        project = 'eclipse'
    elif dir.startswith('mozilla'):
        project = 'mozilla'
    else:
        project = 'apache'
    result_path = os.path.join('./', project.upper(), dir)
    best_result_metric = 0
    best_result = ''
    for file in os.listdir(result_path):
        if os.path.isdir(file):
            continue
        if not file.startswith(model):
            continue

        result=pd.read_csv(os.path.join(result_path,file),index_col=0)
        temp_metric_float=result[metric].astype('float64', copy=True)
        evaluate_metric=np.mean(temp_metric_float)

        if best_result_metric <evaluate_metric:
            best_result_metric=evaluate_metric
            best_result=result
    # print('file:%s, value: %s'%(best_result_file,best_result_metric))
    return best_result.astype('float64', copy=True,errors='ignore')

def average_ten_fold(project,model,filter=False):
    # project='apache'
    # model='TransE'
    file_name = project + '_' + model + '_' + str(filter) + '.csv'
    store_path=os.path.join('./Result',file_name)
    if os.path.exists(store_path):
        return

    metric='mrr'
    accum_sum=0
    dir=os.path.join('./',project.upper())
    for f in os.listdir(dir):
        if os.path.isdir(os.path.join(dir,f)):
            # filter=true represent filter result
            if not filter:
                if 'f' in f:
                    continue
            else:
                if not 'f' in f:
                    continue

            dir_best_result=best_result2dir(f,model,metric)
            if type(accum_sum)==int:
                accum_sum=dir_best_result
            else:
                accum_sum += dir_best_result
    # print(accum_sum)
    fold=10
    if project=='apache':
        fold=2
    accum_sum=accum_sum/fold

    accum_sum.to_csv(store_path)

    print(accum_sum)

# result statistic
def statistic():
    projects=['eclipse','mozilla','apache']
    models=['PTITransE','TransE','TransD','TransH']
    filters=[True,False]
    for project in projects:
        for model in models:
            for filter in filters:
                average_ten_fold(project,model,filter)
```

## Deep learning based methods

In addition, we compare the performance of PTITransE with the state-of-the-art bug triaging methods under the stringent assumption.   To enable a fair comparison,  we reimplement the deep learning based methods and use the obtained results of our own implementation to expediently compare and analyze the performance differences between our method and these baselines. The source code of deep learning based methods are free available from [google driven](https://drive.google.com/drive/folders/1bxHUeXMhOYJ9GzhTOmuvonezWKhOdLEF?usp=sharing).

