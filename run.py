import config
import models
import numpy as np
import json
import os
import pandas as pd
import re


class Run:
    def __init__(self, epoch, batch, dim,alpha):
        # self.project = project
        self.epoch = epoch
        self.bach = batch
        self.dim = dim
        self.alpha=alpha
        self.init_model()

    def init_model(self):
        self.models = {}
        self.models['TransE'] = models.TransE
        self.models['TransH'] = models.TransH
        self.models['TransD'] = models.TransD
        self.models['PTITransE'] = models.PTITransE

    def train(self, train_path, model_path, model):
        # os.environ['CUDA_VISIBLE_DEVICES']='6'
        con = config.Config()
        # Input training files from benchmarks/FB15K/ folder.
        con.set_in_path(train_path)
        # True: Input test files from the same folder.
        con.set_log_on(1)
        con.set_work_threads(8)
        con.set_train_times(self.epoch)
        con.set_nbatches(self.bach)
        con.set_alpha(self.alpha)
        con.set_bern(0)
        con.set_dimension(self.dim)
        con.set_margin(1.0)
        con.set_ent_neg_rate(1)
        con.set_rel_neg_rate(0)
        con.set_opt_method("SGD")
        # Model parameters will be exported via torch.save() automatically.
        con.set_export_files(model_path + ".model.vec.pt")
        # Model parameters will be exported to json files automatically.
        con.set_out_files(model_path + ".embedding.vec.json")
        con.init()
        con.set_model(model)
        con.run()

    def test(self, test_path, model_path, model):
        # (1) Set import files and OpenKE will automatically load models via torch.load().
        con = config.Config()
        con.set_in_path(test_path)
        con.set_developer()
        con.set_test_link_prediction(True)
        con.set_work_threads(4)
        con.set_dimension(self.dim)
        con.set_import_files(model_path + ".model.vec.pt")
        con.init()
        con.set_model(model)
        result = con.test()
        return result

    def getEmbeddingMatrix(self, file_path, model_path, model):
        con = config.Config()
        con.set_in_path(file_path)
        con.set_work_threads(4)
        con.set_dimension(self.dim)
        con.set_import_files(model_path + "model.vec.pt")
        con.init()
        con.set_model(model)
        # Get the embeddings (numpy.array)
        embeddings = con.get_parameters("numpy")
        # Get the embeddings (python list)
        # embeddings = con.get_parameters()
        np.savetxt(file_path + 'embeddings1.txt', embeddings['ent_embeddings.weight'])
        np.savetxt(file_path + 'embeddings2.txt', embeddings['ent_transfer.weight'])

    def experiment(self, model,data_path,model_path):

        result_save_name = '%s_%s_%s_%s_%s.result' % (model, self.epoch, self.dim, self.bach, self.alpha)
        result_save_path = os.path.join(model_path, result_save_name)
        if os.path.exists(result_save_path):
            return None
        else:
            model_save_name_half = '%s' % (model)
            index = ['Classify', 'Rank']
            columns = ['top@' + str(i) for i in range(1, 10 + 1)] + ['mrr', 'mr', 'MAP']
            df = pd.DataFrame(columns=columns, index=index)
            if not os.path.exists(model_path + '/' + model_save_name_half + '.model.vec.pt'):
                # train
                self.train(train_path=data_path + '/', model_path=model_path + '/' + model_save_name_half,
                           model=self.models[model])
            # test
            recall_10_classify, mrr_classify, mr_classify, recall_10_rank, mrr_rank, mr_rank, MAP = self.test(
                test_path=data_path + '/', model_path=model_path + '/' + model_save_name_half, model=self.models[model])

            # remove temp files during training models, which reduces space usage
            os.remove(model_path + '/' + model_save_name_half + '.model.vec.pt')
            os.remove(model_path + '/' + model_save_name_half + '.embedding.vec.json')

            # save result
            recall_10_classify.append(mrr_classify)
            recall_10_classify.append(mr_classify)
            recall_10_classify.append(None)
            recall_10_rank.append(mrr_rank)
            recall_10_rank.append(mr_rank)
            recall_10_rank.append(MAP)

            df.loc['Classify'] = pd.Series(recall_10_classify, index=columns)
            df.loc['Rank'] = pd.Series(recall_10_rank, index=columns)
            df.to_csv(result_save_path)
            print(df)

    # for i in range(10):
    #     train_path=self.project+

    # test(test_path='./benchmarks/1/', model_path='./ECLIPSE/1/', model=models.TransE, model_name='TransE')


def start():
    benchmarks_path = './benchmarks'
    files = os.listdir(benchmarks_path)
    for dir in files:
        # dir = 'eclipse_1'
        data_path = os.path.join(benchmarks_path, dir)
        if dir.startswith('eclipse'):
            project='eclipse'
        elif dir.startswith('mozilla'):
            project='mozilla'
            continue
        else:
            project='apache'
            continue
        if os.path.isdir(data_path):
            model_path = os.path.join('./', project.upper(), dir)
            # create dir if dir is not existed
            if not os.path.exists(model_path):
                os.makedirs(model_path)
            # 'TransE','PTITransE', 'TransH', 'TransD'
            train_models = ['PTITransE']
            for model in train_models:
                # epoch batch dim
                run = Run(300, 200, 100,0.001)
                run.experiment(model,data_path,model_path)

def addjust_parameters():
    benchmarks_path = './benchmarks'
    dir = 'eclipse_1'
    if dir.startswith('eclipse'):
        project = 'eclipse'
    elif dir.startswith('mozilla'):
        project = 'mozilla'
    else:
        project = 'apache'
    data_path = os.path.join(benchmarks_path, dir)
    model_path = os.path.join('./', project.upper(), dir)
    # create dir if dir is not existed
    if not os.path.exists(model_path):
        os.makedirs(model_path)
    train_models = ['PTITransE']
    for model in train_models:
        # epoch batch dim alpha
        epoch_list=[100,200,300]
        batch_list=[100,200,300]
        dim_list=[100,200,300]
        alpha_list=[0.001]
        for epoch in epoch_list:
            for batch in batch_list:
                for dim in dim_list:
                    for alpha in alpha_list:
                        run = Run(epoch, batch, dim, alpha)
                        run.experiment(model, data_path, model_path)


if __name__ == '__main__':
    start()
    # addjust_parameters()
    # train(train_path='./benchmarks/1/', model_path='./ECLIPSE/1/', model=models.TransE, model_name='TransE')
    # test(test_path='./benchmarks/1/', model_path='./ECLIPSE/1/', model=models.TransE, model_name='TransE')
    # getEmbeddingMatrix(model=models.TransD)
